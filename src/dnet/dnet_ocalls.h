/*
 * Created on Fri Feb 14 2020
 *
 * Copyright (c) 2020 Peterson Yuhala, IIUN
 */

#ifndef DNET_OCALLS_H
#define DNET_OCALLS_H

#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "darknet.h"
//#include "../crypto/crypto.h"

#if defined(__cplusplus)
extern "C"
{
#endif
    void ocall_open_file(const char *filename, flag oflag);
    void ocall_close_file();
    void enc_fread(void *ptr, size_t size, size_t nmemb, int dummy);
    void enc_fwrite(void *ptr, size_t size, size_t nmemb, int dummy);
#if defined(__cplusplus)
}
#endif

#endif /* DNET_OCALLS_H */
