/**
 * Author: Peterson Yuhala
 * SGX utils for darknet port
 */

//At the time of this porting Intel SGX does not support CUDA
//Therefore all GPU related functionality has been stripped off
//PYuhala Feb 2020

#ifndef DNET_SGX_UTILS_H
#define DNET_SGX_UTILS_H


#include <stdarg.h>
#include <stdio.h> /* vsnprintf */
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "dnet_ocalls.h"


#define RAND_MAX 2147483647
#define INT_MAX RAND_MAX
#define MAX_CHAR 4096

//#define DNET_SGX_DEBUG //uncomment if you need more verbose output during training/debugging
//#define SGX_OMP //OMP not used yet 

#define DEBUG_PRINT()                                 \
    {                                                 \
        printf("########## DEBUG HERE ##########\n"); \
    }   


#endif //DNET_SGX_UTILS_H
