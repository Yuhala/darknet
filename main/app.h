/*
 * Created on Thu Mar 12 2020
 *
 * Copyright (c) 2020 Peterson Yuhala, IIUN
 */

#ifndef _APP_H_
#define _APP_H_

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "dnet_ocalls.h"
#include "data_mnist.h"
#include "dnet_sgx_utils.h"
#include "darknet.h"


#define CIFAR_WEIGHTS "./backup/cifar.weights"
#define TINY_WEIGHTS "./backup/tiny.weights"
//#define MNIST_WEIGHTS "./backup/mnist.weights"
#define USE_DISK
#define CRASH_TESTx


#define NUM_THREADS 3    //number of worker threads
#define KILO 1024        //1 << 10
#define MEGA KILO * 1024 //1 << 20

//---------------------------------------------------------------------------------
/**
 * Config files
 */

#define CONFIG_BASE "./cfg/big/"

#define MNIST_TRAIN_IMAGES "./data/mnist/train-images-idx3-ubyte"
#define MNIST_TRAIN_LABELS "./data/mnist/train-labels-idx1-ubyte"
#define MNIST_TEST_IMAGES "./data/mnist/t10k-images-idx3-ubyte"
#define MNIST_TEST_LABELS "./data/mnist/t10k-labels-idx1-ubyte"
#define MNIST_CFG "./cfg/mnist.cfg"
#define TEST_CFG "./cfg/cfg.cfg"
#define MNIST_WEIGHTS "./backup/mnist.weights"
//#define NV_MODEL "/dev/shm/romuluslog_shared"
#define CRASH "./results/crash.csv"
#define NOCRASH "./results/nocrash.csv"
#define TRAIN_STEPS "./results/steps.csv"
//////////////////Save/Restore Exp/////////////////////////
#define SAVE_DISK "./results/save_disk.csv"
#define RESTORE_DISK "./results/restore_disk.csv"
#define SAVE_PM "./results/save_pm.csv"
#define RESTORE_PM "./results/restore_pm.csv"


#define CRASH_TESTx

void train_cifar(char *cfgfile);
void test_cifar(char *cfgfile);
void train_mnist(char *cfgfile);
void test_mnist(char *cfgfile);

//for benchmarking purposes
void start_clock();
void stop_clock();
void add_loss();
void run_bench();
void crash_test();
void stop_step(opt bench_step);

#endif /* !_APP_H_ */
