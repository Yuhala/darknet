/*
 * Created on Thu Mar 12 2020
 *
 * Copyright (c) 2020 Peterson Yuhala, IIUN
 */

#include <stdio.h>
#include <string.h>
#include <pwd.h>
#include <thread>

#include "app.h"

/* Darknet variables */
static data training_data, test_data;
static network* net;

/* Benchmarking */
#define BENCHMARKING
#include "benchtools.h"
#include <time.h>
struct timespec start, stop;
double diff;
bench_info *bench_point;
std::vector<double> sup_times;
std::vector<double> chkpt_times;
std::vector<double> loss;
std::vector<int> epoch;
//Mirroring steps
std::vector<double> read_times;
std::vector<double> dec_times;
std::vector<double> enc_times;
std::vector<double> write_times;

//Other variables for tests
static bool pm_empty = true;
static bool disk_empty = true;

/* For benchmarking */
//check if file is empty
bool is_empty(std::ifstream &pFile)
{
    return pFile.peek() == std::ifstream::traits_type::eof();
}
//clearing time vectors
void clear_vectors()
{
    read_times.clear();
    write_times.clear();
    enc_times.clear();
    dec_times.clear();
}
void stop_step(opt bench_step)
{
}
void register_vals(int test)
{
}

void start_clock()
{
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
}
void stop_clock()
{
    clock_gettime(CLOCK_MONOTONIC_RAW, &stop);
}

void run_bench()
{

    bench_point = (bench_info *)malloc(sizeof(bench_info));
    std::string img_path = MNIST_TRAIN_IMAGES;
    std::string label_path = MNIST_TRAIN_LABELS;
    training_data = load_mnist_images(img_path);
    training_data.y = load_mnist_labels(label_path);

    printf("Benchmark progress: 0%%\n");

    for (int i = 5; i <= 50; i += 5)
    { //we have 10 config files in total
        //choose config file
        char base[] = CONFIG_BASE;
        char cfg[128];
        snprintf(cfg, sizeof(cfg), "%scfg%d.cfg", base, i);
        //printf("cfg: %s\n", cfg);

        //part a: bench param saving
        bench_point->chkpt = 1;

        printf("Save bench: \n");
        train_mnist(cfg);

        remove(MNIST_WEIGHTS);
        printf("Benchmark progress: %d%%\n", i * 2);
    }
    //close results files
    //restFile.close();
    //saveFile.close();
}

//------------------------------------------------------------------------------------------------------------------------

void train_mnist(char *cfgfile)
{
#undef BENCHMARKING
#ifndef BENCHMARKING
    std::string img_path = MNIST_TRAIN_IMAGES;
    std::string label_path = MNIST_TRAIN_LABELS;
    training_data = load_mnist_images(img_path);
    training_data.y = load_mnist_labels(label_path);
    bench_point = (bench_info *)malloc(sizeof(bench_info));
#endif

    list *sections = read_cfg(cfgfile);

    printf("Training mnist model..\n");
    net = NULL;
    //printf("Done creating network in enclave...\n");

    srand(12345);
    float avg_loss = -1;
    float loss = 0;
    int classes = 10;
    int N = 60000; //number of training images
    int cur_batch = 0;
    float progress = 0;
    int count = 0;
    data train = training_data;
    char *path = MNIST_WEIGHTS;
    unsigned int num_params;
    bench_point->chkpt = 1;

    net = create_net_in(sections);
    /*  num_params = get_param_size(net);
    printf("Number of params: %d\n",num_params);
    double net_size = num_params * sizeof(float)/KILO; */

    int epoch = (*net->seen) / N;
    count = 0;
    printf("Max batches: %d\n", net->max_batches);

    //benchmark model saving
    while ((cur_batch < net->max_batches || net->max_batches == 0))
    {
        //number of iterations for a single benchmarking point.
        cur_batch = get_current_batch(net);

        loss = train_network_sgd(net, train, 1);

        if (avg_loss == -1){
            avg_loss = loss;
        }
            
        avg_loss = avg_loss * .95 + loss * .05;
        epoch = (*net->seen) / N;

        progress = ((double)cur_batch / net->max_batches) * 100;
        printf("Batch num: %ld, Seen: %.3f: Loss: %f, Avg loss: %f avg, L. rate: %f, Progress: %.2f%% \n",
               cur_batch, (float)(*net->seen) / N, loss, avg_loss, get_current_rate(net), progress);

        //start_clock();
        save_weights(net, path);
        //stop_clock();
    }
    num_params = get_param_size(net);
    bench_point->model_size = (double)(num_params * 4) / (1024 * 1024);
    //register_vals(1);
    printf("registered values\n");

    //point->model_size = num_params;
    printf("Number of params: %d Model size: %f\n", num_params, bench_point->model_size);
    printf("Done training mnist network..\n");
    free_network(net);
}

/**
 * Test a trained mnist model
 * Define path to weightfile in trainer.c
 */
void test_mnist(char *cfgfile)
{

    std::string img_path = MNIST_TEST_IMAGES;
    std::string label_path = MNIST_TEST_LABELS;
    data test = load_mnist_images(img_path);
    test.y = load_mnist_labels(label_path);
    list *sections = read_cfg(cfgfile);



    char *weightfile = MNIST_WEIGHTS;
    load_network(net, MNIST_WEIGHTS, 0);
    //net = create_net_in(sections);   
    srand(12345);

    float avg_acc = 0;  
    float *acc = network_accuracies(net, test, 2);
    avg_acc += acc[0];

    printf("Accuracy: %f%%, %d images\n", avg_acc * 100, test.X.rows);
    free_network(net);

    //ecall_tester(sections, &test, 0);
    printf("Mnist testing complete..\n");
    free_data(test);
}

/* Application entry */
int main(int argc, char *argv[])
{

    //Create NUM_THRREADS threads
    //std::thread trd[NUM_THREADS];
    //test_mnist(MNIST_CFG);
    /*  
    for (int i = 0; i < NUM_THREADS; i++)
    {
        trd[i] = std::thread(thread_func);
    }
    for (int i = 0; i < NUM_THREADS; i++)
    {
        trd[i].join();
    } */

    /* Benchmarking stuff */
    start_clock();
    train_mnist(MNIST_CFG);
    stop_clock();
    double diff = time_diff(&start, &stop, MILLI);
    //run_bench();
    //crash_test();

    return 0;
}
