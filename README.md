# Summary
-This repo contains `Darknet ML library`. This is a slightly modified version (e.g no CUDA) of the original repo [here](https://github.com/pjreddie/darknet).

# Background on darknet
- Darknet uses config files `xx.cfg` to define the structure of a neural network model. Examples of config files are found in the `config` directory. 
- The config file contains the model's hyperparameters, e.g: number of layers and their activations, batch size, learning rates, max batches (or iterations) for full training of the model.
- The [mnist.cfg](cfg/mnist.cfg) for example describes a neural network with 4 convolutional layers, batch size of 128, max batches of 500 (i.e model will train for 500 iterations), and other intermediate layers.
- During training the training algorithm iterates through the training data and updates the model until it goes through all its iterations.
- For a supervised training algorithm, like the case with MNIST, the dataset comes in pairs. E.g image and label. 
- To train a model, create the corresponding config file and add the training data in the data folder. This repo already contains the entire MNIST data set.
- For more background info on Darknet visit their main website [here](https://pjreddie.com/darknet/)

# How to train a model 
- The instructions here are valid for linux based systems.
## Building a darknet application. 
- You can build darknet as a shared library or integrate the source code directly in your project. Here we use the latter approach.
- Clone this repo to your system: `git clone https://gitlab.com/Yuhala/darknet.git`.
- An example training application has already been written in [app.cpp](main/app.cpp). The algorithm trains a model on the MNIST data set (which you can find in the `data` folder). 
- Build the program with `make`. If it builds successfully run it with `./app`.
- As the model trains, you can see the `loss` and `average loss` decreasing. The formula for `avg loss` is from the Darknet repo.
- To save the model's weights to disk during training, use `save_weights(net, path)`. Saved weights can be loaded with  `load_network(net, MNIST_WEIGHTS, 0)`.
- You can test the accuracy of the model with the `test_mnist` routine in [app.cpp](main/app.cpp).


## Other useful info
- Measuring the time for each MNIST iteration. Attach you timer here: `loss = train_network_sgd(net, train, 1);` in [app.cpp](main/app.cpp).
- Measuring the total training time for MNIST. Time the routine `train_mnist(MNIST_CFG);` in the main function. That has already been done.
- Measuring the time to load the MNIST dataset (i.e images and labels), time these two routines: 
```
data test = load_mnist_images(img_path);
test.y = load_mnist_labels(label_path);
```
- There is no option to define an absolute size (e.g 1MB) for a model. You can vary the model's size by varying the number of layers (for the MNIST example add/remove some of the convolutional layers), or the filter sizes layers. The higher the `filter` value for a layer the higher the number of neurons and the larger its size and vice versa.

# Contact
- Author: Peterson Yuhala
- This repo was "rush created" and my instructions may not be clear enough. If you need any clarifications contact me: `petersonyuhala@gmail.com`