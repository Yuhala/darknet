#
# Author: Peterson Yuhala
# Docker file to build scone-dnet project in a scone container
#

# Download base image
FROM sconecuratedimages/crosscompilers:alpine3.7

# Branch to checkout
ENV BRANCH alpine
# Define enclave heap size
ENV SCONE_HEAP 4G
# Use SCONE crosscompiler
ENV CC scone-gcc
ENV CXX scone-g++

RUN apk update \
    && apk add --no-cache --virtual=.builddeps \
    bash \
    build-base \
    linux-headers \
    make \
    musl-dev \
    git \
    wget

RUN apk --no-cache add \
    libc6-compat \
    libexecinfo \
    libexecinfo-dev \
    libressl-dev \
    libunwind \
    libunwind-dev \
    vim


# Create base dir for sgx-dnet-romulus
RUN mkdir -p /home/ubuntu/peterson \
        && mkdir /work && mkdir -p /mnt/pmem4

RUN cd /home/ubuntu/peterson && git clone https://gitlab.com/Yuhala/scone-dnet.git 


    
#CMD ["python3"]
