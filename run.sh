#!/bin/bash

#SCONE_VERSION=1 ./app
results_dir=./results
shm_dir=/dev/shm
pmem_dir=/mnt/pmem4
back_dir=./backup

sudo docker run  --shm-size 1G -it --privileged \
 -v `pwd`/results:$results_dir \
 -v `pwd`/backup:$back_dir \
 -v /dev/shm:$shm_dir \
  scone-dnetv2 bash

