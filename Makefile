#
# * Created on Thu Mar 12 2020
# *
# * Copyright (c) 2020 Peterson Yuhala, IIUN
#

CXX := g++
CC := gcc

#--------------------------------------------------------
# dnet object directories
DNET_BASE := src/dnet
SRC_BASE := src
APP_BASE := main

DNET_INC := -I$(DNET_BASE) -I$(SRC_BASE)/include 


# dnet objects
OBJ := activation_layer.o activations.o avgpool_layer.o batchnorm_layer.o blas.o box.o col2im.o \
	connected_layer.o convolutional_layer.o cost_layer.o crnn_layer.o crop_layer.o deconvolutional_layer.o \
	detection_layer.o dropout_layer.o gemm.o gru_layer.o im2col.o iseg_layer.o l2norm_layer.o layer.o \
	list.o local_layer.o logistic_layer.o lstm_layer.o matrix.o maxpool_layer.o normalization_layer.o \
	region_layer.o reorg_layer.o rnn_layer.o route_layer.o shortcut_layer.o softmax_layer.o tree.o \
	upsample_layer.o utils.o image.o yolo_layer.o network.o data.o option_list.o parser.o dnet_ocalls.o data_mnist.o


DNET_OBJS := $(addprefix $(DNET_BASE)/, $(OBJ)) 

DNET_DEPS := $(wildcard $(DNET_BASE)/*.h) $(wildcard $(SRC_BASE)/include/*.h)


App_Name := app

App_Cpp_Files := $(wildcard main/*.cpp)  $(wildcard src/bench/*.cpp) 
App_Cpp_Objects := $(App_Cpp_Files:.cpp=.o) 

App_Cpp_Flags := -std=c++11
APP_INC := $(DNET_INC) -I$(APP_BASE) -I$(SRC_BASE)  -I$(SRC_BASE)/bench  

App_Link_Flags := -lpthread -lm -lssl -lcrypto

.PHONY: all run 

all: $(App_Name) 

$(DNET_BASE)/%.o: $(DNET_BASE)/%.c $(DNET_DEPS)
	@$(CC) $(DNET_INC) $(App_Cpp_Flags) -c $< -o $@
	@echo "CC  <=  $<"

$(DNET_BASE)/%.o: $(DNET_BASE)/%.cpp $(DNET_DEPS)
	@$(CXX) $(DNET_INC) $(App_Cpp_Flags) -c $< -o $@
	@echo "CXX  <=  $<"

$(SRC_BASE)/bench/%.o: $(SRC_BASE)/bench/%.cpp $(SRC_BASE)/bench/%.h
	@$(CXX) $(App_Cpp_Flags) -c $< -o $@
	@echo "CXX  <=  $<"



$(APP_BASE)/%.o: $(APP_BASE)/%.cpp
	@$(CXX) $(APP_INC) $(App_Cpp_Flags) -c $< -o $@
	@echo "CXX  <=  $<"

$(App_Name): $(DNET_OBJS) $(App_Cpp_Objects)  $(Rom_Obj) #$(Other_Objects)
	@$(CXX) $^ -o $@ $(App_Link_Flags) 
	@echo "LINK =>  $@"


.PHONY: clean

clean: 
	@rm -f $(App_Name) $(App_Cpp_Objects) $(DNET_BASE)/*.o ./*.o

	






